module github.com/nortonlifelock/funnel

go 1.13

require (
	github.com/benjivesterby/validator v1.0.1-0.20200101181545-cfd93a3612a2
	github.com/nortonlifelock/aegis v1.0.1-0.20200214181401-3a5d0fe8c62c // indirect
	github.com/nortonlifelock/log v1.0.1-0.20200129171320-c4a4dd839ed8
	github.com/pkg/errors v0.9.1
)
