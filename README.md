# funnel

[![Build Status](https://api.travis-ci.org/nortonlifelock/funnel.svg?branch=master)](https://travis-ci.org/nortonlifelock/funnel)
[![Go Report Card](https://goreportcard.com/badge/github.com/nortonlifelock/funnel)](https://goreportcard.com/report/github.com/nortonlifelock/funnel)
[![GoDoc](https://godoc.org/github.com/nortonlifelock/funnel?status.svg)](https://godoc.org/github.com/nortonlifelock/funnel)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](http://makeapullrequest.com)

funnel
